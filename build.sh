#/bin/bash

# resolve links - $0 may be a softlink
PRG="$0"

if [ -z "$COINSTACK_ASSEMBLY_HOME" ]; then
  while [ -h "$PRG" ] ; do
    ls=`ls -ld "$PRG"`
    link=`expr "$ls" : '.*-> \(.*\)$'`
    if expr "$link" : '/.*' > /dev/null; then
      PRG="$link"
    else
      PRG=`dirname "$PRG"`/"$link"
    fi
  done

  cd $(dirname $PRG)
  export COINSTACK_ASSEMBLY_HOME=`pwd`
  cd -&>/dev/null
fi

. $COINSTACK_ASSEMBLY_HOME/src/assembly/bin/common

declare -x OS_FAMILY=`uname | tr '[:upper:]' '[:lower:]'`
declare -x OS_MACHINE=`uname -m | tr '[:upper:]' '[:lower:]'`
[ -z "$OS_FAMILY" ] && error 'Unknown os family'

. $COINSTACK_ASSEMBLY_HOME/versions

declare -x BTCD_VERSION=${BTCD_VERSION:-'3.3.1'}
declare -x ANDROID_FINGERPRINT_VERSION=${ANDROID_FINGERPRINT_VERSION:-'1.0.0'}
declare -x COINSTACK_VERSION=${COINSTACK_VERSION:-'3.3.1'}
declare -x COINSTACK_INSTALLER=coinstack-$OS_FAMILY-$OS_MACHINE-$COINSTACK_VERSION.bin

declare -x COINSTACK_SRC=${COINSTACK_SRC:-$COINSTACK_ASSEMBLY_HOME/src}
declare -x COINSTACK_PACKAGE_SRC=${COINSTACK_PACKAGE_SRC:-$COINSTACK_SRC/assembly}
declare -x COINSTACK_INSTALLER_SRC=${COINSTACK_INSTALLER_SRC:-$COINSTACK_SRC/installer}
declare -x COINSTACK_PROVISION_SRC=${COINSTACK_PROVISION_SRC:-$COINSTACK_SRC/provision}
declare -x BUILD_WORKSPACE=${BUILD_WORKSPACE:-$COINSTACK_ASSEMBLY_HOME/workspace}
declare -x BUILD_TARGET=${BUILD_TARGET:-$COINSTACK_ASSEMBLY_HOME/target}
declare -x PACKAGE_ROOT=${PACKAGE_ROOT:-$BUILD_TARGET/coinstack-$COINSTACK_VERSION}
declare -x DIST_TARGET=${DIST_TARGET:-$COINSTACK_ASSEMBLY_HOME/dist}

declare -x BTCD_GIT_USER=${BTCD_GIT_USER:-'git'}
declare -x BTCD_GIT_SERVER=${BTCD_GIT_SERVER:-'bitbucket.org'}
declare -x BTCD_GIT_URL=${BTCD_URL:-"$BTCD_GIT_USER@$BTCD_GIT_SERVER:/cloudwallet/btcd.git"}

. $COINSTACK_PACKAGE_SRC/bin/common

function install-prerequisites() {
  inform "Preparing directories... : $BUILD_WORKSPACE $BUILD_TARGET"
  sudo yum install -y golang git zip unzip
}

function clean() {
  inform "Cleaning working directories..."
  rm -rf $BUILD_WORKSPACE $BUILD_TARGET
}
function ensure-git() {
  (which aptitude &>/dev/null && sudo aptitde install -y git) ||\
    (which yum &>/dev/null && sudo yum install -y git)
}

function ensure-golang() {
  (which aptitude &>/dev/null && sudo aptitde install -y golang) ||\
    (which yum &>/dev/null && sudo yum install -y golang)
}

function ensure-zip() {
  (which aptitude &>/dev/null && sudo aptitde install -y zip) ||\
    (which yum &>/dev/null && sudo yum install -y zip)
}

function ensure-java() {
  sudo add-apt-repository -y ppa:webupd8team/java
  sudo apt-get update -y
  echo "debconf shared/accepted-oracle-license-v1-1 select true" | sudo debconf-set-selections
  echo "debconf shared/accepted-oracle-license-v1-1 seen true" | sudo debconf-set-selections
  sudo apt-get install -y oracle-java8-installer
}
function ensure-android() {
  cd $BUILD_WORKSPACE
  ZIP_NAME=sdk-tools-linux-3859397.zip
  wget https://dl.google.com/android/repository/$ZIP_NAME
  unzip $ZIP_NAME

  ANDROID_HOME=$BUILD_WORKSPACE/android-sdk
  ANDROID_TOOL=$ANDROID_HOME/tools
  mkdir $ANDROID_HOME
  unzip $ZIP_NAME -d $ANDROID_HOME

  export ANDROID_HOME=`pwd`
  echo "y" | $ANDROID_TOOL/bin/sdkmanager "platforms;android-26"
}

function build() {
  inform "Preparing directories... : $BUILD_WORKSPACE $BUILD_TARGET"
  clean
  mkdir -p $BUILD_WORKSPACE $BUILD_TARGET
  build-btcd
  package-zip
  build-installer
}

function package-zip() {
  inform "Building package..."
  ensure-zip
  cat<<EOF >$PACKAGE_ROOT/doc/version.txt
v$COINSTACK_VERSION-$(date +"%Y%m%d%H%M%S")
btcd v$BTCD_VERSION $BTCD_COMMIT_ID

EOF

  cd $BUILD_TARGET
  mkdir -p $COINSTACK_ASSEMBLY_HOME/dist
  zip -r $BUILD_WORKSPACE/coinstack.zip .
  cat <<EOF | cat - $PACKAGE_ROOT/bin/common $COINSTACK_INSTALLER_SRC/install-to-linux.sh $BUILD_WORKSPACE/coinstack.zip > $DIST_TARGET/$COINSTACK_INSTALLER
#!/bin/bash
declare -x VERSION=$COINSTACK_VERSION
EOF
  chmod 755 $DIST_TARGET/$COINSTACK_INSTALLER
}

function build-btcd() {
  ensure-git
  ensure-golang

  # Build btcd
  export GOPATH=$BUILD_WORKSPACE
  go env GOPATH

  debug "Getting glide..."
  go get -u github.com/Masterminds/glide

  debug "Getting btcd from $BTCD_GIT_URL..."
  git clone $BTCD_GIT_URL $GOPATH/src/github.com/btcsuite/btcd

  cd $GOPATH/src/github.com/btcsuite/btcd
  git checkout v$BTCD_VERSION
  export BTCD_COMMIT_ID=`git rev-parse HEAD`

  debug "Building btcd..."
  $GOPATH/bin/glide install
  go install . ./cmd/...

  inform "Making directory structure..."
  mkdir -p $PACKAGE_ROOT
  cp -r $COINSTACK_PACKAGE_SRC/* $PACKAGE_ROOT
  mkdir -p $PACKAGE_ROOT/lib
  cp $GOPATH/bin/btcd $PACKAGE_ROOT/lib
  cd -
}

function build-android-fingerprint-keychain() {
  inform "Preparing directories... : $BUILD_WORKSPACE $BUILD_TARGET"
  ensure-git

  mkdir -p $BUILD_WORKSPACE $BUILD_TARGET
  git clone git@bitbucket.org:cloudwallet/android-fingerprint-keychain.git $BUILD_WORKSPACE/android-fingerprint-keychain
  cd $BUILD_WORKSPACE/android-fingerprint-keychain
  git checkout v$ANDROID_FINGERPRINT_VERSION
  export ANDROID_FINGERPRINT_COMMIT_ID=`git rev-parse HEAD`
  $BUILD_WORKSPACE/android-fingerprint-keychain/gradlew build
  mkdir -p $PACKAGE_ROOT/ext
  cp $BUILD_WORKSPACE/android-fingerprint-keychain/coinstack-fingerprint/build/outputs/aar/coinstack-fingerprint-release.aar $PACKAGE_ROOT/ext/coinstack-fingerprint-1.0.0.aar
  cd -
}

function build-installer() {
  inform "Building installer..."
  cat $COINSTACK_PACKAGE_SRC/bin/common $COINSTACK_INSTALLER_SRC/install-to-universial.sh >$DIST_TARGET/coinstack-installer.sh
  chmod 777 $DIST_TARGET/coinstack-installer.sh
}

function build-installer-on() {
  pushd . >>/dev/null
  export BOX_IMAGE=$1
  COINSTACK_VAGRANT=$BUILD_WORKSPACE/vagrant

  inform "Stopping vagrant..."
  cd $COINSTACK_VAGRANT &>/dev/null && vagrant destroy -f
  rm -rf $COINSTACK_VAGRANT
  mkdir -p $COINSTACK_VAGRANT/coinstack-assembly
  cp -r $COINSTACK_SRC $COINSTACK_ASSEMBLY_HOME/build.sh $COINSTACK_ASSEMBLY_HOME/versions $COINSTACK_VAGRANT/coinstack-assembly
  cat $COINSTACK_PROVISION_SRC/vagrant/Vagrantfile.template | envsubst >$COINSTACK_VAGRANT/Vagrantfile

  inform "Starting $BOX_IMAGE..."
  cd $COINSTACK_VAGRANT
  HOSTNAME=`vagrant up | grep guestaddress | awk '{ print $4 }'`
  echo $HOSTNAME
  ssh $HOSTNAME <<EOF
  export COINSTACK_VERSION=$COINSTACK_VERSION
  cp -r /vagrant/coinstack-assembly .
  cd coinstack-assembly
  ./build.sh
EOF
  scp $HOSTNAME:coinstack-assembly/dist/coinstack-* $COINSTACK_ASSEMBLY_HOME/dist
  inform "Stopping vagrant..."
  cd $COINSTACK_VAGRANT &>/dev/null && vagrant destroy -f
  popd >>/dev/null
}

COMMAND=$1
shift

case "$COMMAND" in
  "clean")
    clean
    ;;
  "installer")
    clean
    build-installer
    ;;
  "build")
    case "$1" in
      "--all")
        build-installer-on "ubuntu/xenial64"
        build-installer-on "ubuntu/xenial32"
        #build-installer-on "jhcook/macos-sierra"
        build
        ;;
      "")
        build
        ;;
      *)
        error "Unknown argument: $1"
    esac
    ;;
  "")
    build
    ;;
  "--all")
    build-installer-on "ubuntu/xenial64"
    build-installer-on "ubuntu/xenial32"
    build
    ;;
  "*")
    ;;
esac
