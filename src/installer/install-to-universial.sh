#!/bin/bash

################################################################################
# Main process
inform "Parsing options..."
DISTRIBUTION=`mktemp`
VERSION_FILE=`mktemp`
declare -x OS_FAMILY=`uname | tr '[:upper:]' '[:lower:]'`
declare -x OS_MACHINE=`uname -m | tr '[:upper:]' '[:lower:]'`

ARGS=""
while [ "$#" -gt 0 ]; do
  case "$1" in
    "--versions")
      download https://blocko.blob.core.windows.net/coinstack/versions.txt $VERSION_FILE
      RECENT_VERSION=`tail -1 $VERSION_FILE`
      cat $VERSION_FILE
      exit 0
      ;;
    "-v"|"--version")
      VERSION=$2
      shift
      ;;
    *)
      ARGS="$ARGS $1"
      ;;
  esac
  shift
done

if [ -z "$VERSION" ]; then
  if [ -z "$RECENT_VERSION" ]; then
    download https://blocko.blob.core.windows.net/coinstack/versions.txt $VERSION_FILE
    VERSION=`tail -1 $VERSION_FILE`
  else
    VERSION="$RECENT_VERSION"
  fi
fi

inform "Downloading coinstack distribution..."
DOWNLOAD_FILENAME="coinstack-$OS_FAMILY-$OS_MACHINE-$VERSION.bin"
DOWNLOAD_URL="https://blocko.blob.core.windows.net/coinstack/$DOWNLOAD_FILENAME"

download $DOWNLOAD_URL $DISTRIBUTION

inform "Checking distribution..."
NOT_FOUND=`head -1 $DISTRIBUTION|grep BlobNotFound`
if [ -n "$NOT_FOUND" ]; then
  error "$DOWNLOAD_URL not found. Check platform or version"
fi

chmod 755 $DISTRIBUTION
inform "Installing coinstack..."

$DISTRIBUTION $ARGS

rm $DISTRIBUTION

exit 0
