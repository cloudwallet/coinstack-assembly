#/bin/bash

declare -x INSTALL_TYPE='local'
declare -x SILENT=0
declare -x TEND_TO_PROCESS=0

################################################################################
# Ensure to use apt-get
function ensure-aptitude() {
  which apt-get &>/dev/null && sudo apt-get update -y >/dev/null
  return $?
}
################################################################################
# Ensure to use yum
function ensure-yum() {
  which yum &>/dev/null && sudo yum update -y >/dev/null
  return $?
}

################################################################################
# Check an existence of unzip, or exit
function ensure-unzip() {
  message-on "Checking unzip..."
  which unzip &>/dev/null
  if [ "$?" -ne 0 ]; then 
    warn "FAIL"
    if [ -t 1 ]; then
      if [ "$SILENT" == 0 ]; then
        question "Installer needs unzip but you don't have it. Could you permit to install unzip before progress? (Y/n)"
        read ANSWER
        shopt -s nocasematch
        case "${ANSWER,,}" in
          ""|"y"|"yes")
            INSTALL_UNZIP=1
            ;;
          "n"|"no")
            INSTALL_UNZIP=0
            error "The installation is stopped by user"
            ;;
          *)
            error "Your answer is not understood. Stop the installation"
            ;;
        esac
      else # [ "$SILENT" == 0 ]
        INSTALL_UNZIP=$TEND_TO_PROCESS
      fi # [ "$SILENT" == 0 ]
    else
      INSTALL_UNZIP=$TEND_TO_PROCESS
    fi
    if [ $INSTALL_UNZIP == 0 ]; then
      error "Installation is failed because unzip doesn't exist. Install unzip and retry."
    else
      message-on 'Installing unzip...'
      (ensure-aptitude && sudo apt-get install -y unzip $APT_OPTS) || (ensure-yum && sudo yum install -y unzip $YUM_OPTS)
      if [ "$?" -ne 0 ]; then
        debug "COMMAND: (ensure-aptitude && sudo apt-get install -y unzip $APT_OPTS) || (ensure-yum && sudo yum install -y unzip $YUM_OPTS)"
        error "Fail to install unzip!! Check your package manager."
      fi
      inform 'OK'
    fi
  else
    inform "OK"
  fi # [ "$?" -ne 0 ]
}
function ensure-install-type() {
  case "$1" in
    "local" | "system" | "service")
      inform "Installation type: $1"
      ;;
    *)
      error "Unknown installation type: $1"
      ;;
  esac
}

################################################################################
# Install locally
function install-local() {
  ensure-unzip

  message-on "Unarchieving to $TARGET..."
  unzip -o -d $TARGET $0 &>/dev/null
  inform "OK"
}

################################################################################
# Install locally
function install-system() {
  install-local
  # Make link
  pushd $TARGET >/dev/null
  TARGET_PATH=`pwd`
  popd >/dev/null
  ln -s $COINSTACK_HOME /usr/share/coinstack
  ln -s $COINSTACK_HOME/bin/coinstack-cli /usr/bin/coinstack-cli

  echo 'export USER_COINSTACK_HOME=$HOME/.coinstack.d' > /etc/profile.d/coinstack.sh
}

################################################################################
# Install as service
function install-service() {
  install-system || error "Fail to install to system!"
  echo 'export USER_COINSTACK_HOME=$HOME/.coinstack.d' > /etc/profile.d/coinstack.sh
  ln -s $COINSTACK_HOME/conf /etc/coinstack
  cp $COINSTACK_HOME/doc/template/coinstack.service /etc/init.d/coinstack
  chmod 755 /etc/init.d/coinstack
  which chkconfig && chkconfig --add coinstack
  which update-rc.d && update-rc.d coinstack defaults && update-rc.d coinstack enable
  service coinstack start
}

function configure-coinstack() {
  NODE_PREFIX=${NODE_PREFIX:-'node-'}
  NODES=${NODES:-0}
  MINING_ADDRESS=${MINING_ADDRESS:-'1Lpr7QjM1fLkpoc8SJkXrj81Wc8VLEWobA'}
  $COINSTACK_HOME/bin/coinstack-cli configure -p $NODE_PREFIX -s $NODES -a $MINING_ADDRESS
}

################################################################################
# Main process
while getopts :a:cd:n:p:st:y optname; do
  debug "Option $optname set with value ${OPTARG}"
  case "$optname" in
    "a")
      export MINING_ADDRESS="$OPTARG"
      ;;
    "c")
      export POST_INSTALL=1
      ;;
    "d")
      TARGET=$OPTARG
      ;;
    "n")
      export NODES="$OPTARG"
      ;;
    "p")
      export NODE_PREFIX="$OPTARG"
      ;;
    "s")
      SILENT=1
      ;;
    "t")
      INSTALL_TYPE="$OPTARG"
      ;;
    "y")
      SILENT=1
      TEND_TO_PROCESS=1
      ;;
    *)
      error "Unknown options: $ARG"
      ;;
  esac
done
if [ -z "$TARGET" ]; then
  case "$INSTALL_TYPE" in
    "local")
      inform "Installation type: $INSTALL_TYPE"
      TARGET=`pwd`
      ;;
    "system" | "service")
      inform "Installation type: $INSTALL_TYPE"
      TARGET=/usr/share
      ;;
    *)
      error "Unknown installation type: $1"
      ;;
  esac
fi
export TARGET
export COINSTACK_HOME="$TARGET/coinstack-$VERSION"

if [ "$SILENT" == 1 ]; then
  APT_OPTS="-q"
  YUM_OPTS="-q"
fi

ensure-install-type $INSTALL_TYPE

install-$INSTALL_TYPE

if [ -n "$POST_INSTALL" ] ; then
  configure-coinstack
fi

inform "Success to install the coinstack!!"

cat $COINSTACK_HOME/doc/template/welcome-install.txt

# Don't remove next statement
# It makes the separation with zip part
exit 0
